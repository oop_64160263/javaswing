package com.worachet.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExsample {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JButton button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setTitle("First JFrame");
        frame.setLayout(null);;
        frame.add(button);
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
