package com.worachet.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;
class Myapp{
    JFrame frame;
    JButton button;
    public Myapp(){
        frame = new JFrame();
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setTitle("First JFrame");
        frame.setLayout(null);;
        frame.add(button);
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

public class Simple {
    public static void main(String[] args) {
        Myapp app = new Myapp();

    }
}
